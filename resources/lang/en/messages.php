<?php

return[
    'translates' => 'Translates',
    'view_translate' => 'Show translate',
    'add_word' => 'Add word to translate',
    'add' => 'Add',
    'translations' => 'Translations',
    'ru' => 'Russian',
    'en' => 'English',
    'ja' => 'Japan',
    'fr' => 'France',
    'de' => 'German',
    'update' => 'Update translations',
    'back' => 'Back',
];
