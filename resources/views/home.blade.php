@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-around">
@foreach($words as $word)
        <div class="card" style="width: 18rem; margin-top: 20px;">
            <div class="card-body">
                <h5 class="card-title">{{$word->getRu()}}</h5>
                <a href="{{route('show-word',$word->id)}}" class="btn btn-primary">@lang('messages.view_translate')</a>
            </div>
        </div>
    @endforeach
    </div>
    @can('display',\App\User::class)
    <form style="margin-top: 50px"  action="{{route('store-word')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="ru">@lang('messages.add_word')</label>
            <input name="ru" type="text" class="form-control @error('en') is-invalid @enderror" id="ru">
        </div>
        @error('ru')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">@lang('messages.add')</button>
    </form>
        @endcan
</div>
@endsection
