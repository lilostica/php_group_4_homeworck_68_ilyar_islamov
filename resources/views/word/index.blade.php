@extends('layouts.app')
@section('content')
    <div class="container">
    <h1>@lang('messages.translations')</h1>
        @can('displayWords',$word)
        @can('display',\App\User::class)
        <form action="{{route('update-word',$word->id)}}" method="post">
            @method('put')
            @csrf
            <div class="form-group">
                <label for="ru">@lang('messages.ru')</label>
                <input name="ru" value="{{ $word->getRU() }}" type="text" class="form-control @error('ru') is-invalid @enderror" id="ru">
                @error('ru')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="en">@lang('messages.en')</label>
                <input name="en" value="{{ $word->getEN() }}" type="text" class="form-control @error('en') is-invalid @enderror" id="ru">
                @error('en')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="de">@lang('messages.de')</label>
                <input name="de" value="{{ $word->getDE() }}" type="text" class="form-control @error('de') is-invalid @enderror" id="de">
                @error('de')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="fr">@lang('messages.fr')</label>
                <input name="fr" value="{{ $word->getFR() }}" type="text" class="form-control @error('fr') is-invalid @enderror" id="fr">
                @error('fr')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ja">@lang('messages.ja')</label>
                <input name="ja" value="{{ $word->getJA() }}" type="text" class="form-control @error('ja') is-invalid @enderror" id="ja">
                @error('ja')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <a href="{{route('home')}}">@lang('messages.back')</a>-
            <button type="submit" class="btn btn-primary">@lang('messages.update')</button>
        </form>
            @endcan
        @endcan
        @cannot('displayWords',$word)
        @cannot('display',\App\User::class)
        <h3>@lang('messages.ru'): {{$word->getRU()}}</h3>
        <h3>@lang('messages.en'): {{$word->getEn()}}</h3>
        <h3>@lang('messages.de'): {{$word->getDE()}}</h3>
        <h3>@lang('messages.fr'): {{$word->getFR()}}</h3>
        <h3>@lang('messages.ja'): {{$word->getJA()}}</h3>
            @endcan
            @endcan
    </div>
    @endsection
