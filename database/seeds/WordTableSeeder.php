<?php

use App\Word;
use Illuminate\Database\Seeder;

class WordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_1 = [
            'user_id' => 1,
            'ru' => ['word' => 'Снег'],
            'en' => ['word' => 'Show'],
            'fr' => ['word' => 'Neige'],
            'ja' => ['word' => '雪(Yuki)'],
            'de' => ['word' => 'Schnee']
            ];
        $word_1 =  Word::create($data_1);

        $data_2 = [
            'user_id' => 2,
            'ru' => ['word' => 'Небо'],
            'en' => ['word' => 'Sky'],
            'fr' => ['word' => 'Сiel'],
            'ja' => ['word' => '空(Sora)'],
            'de' => ['word' => 'Himmel']
        ];
        $word_2 =  Word::create($data_2);

        $data_3 = [
            'user_id' => 3,
            'ru' => ['word' => 'Машина'],
            'en' => ['word' => 'Сar'],
            'fr' => ['word' => 'Une machine'],
            'ja' => ['word' => '機械(Kikai)'],
            'de' => ['word' => 'Auto']
        ];
        $word_3 =  Word::create($data_3);

        $data_4 = [
            'user_id' => 4,
            'ru' => ['word' => 'Стол'],
            'en' => ['word' => 'Table'],
            'fr' => ['word' => 'Table'],
            'ja' => ['word' => 'テーブル(Tēburu)'],
            'de' => ['word' => 'Tisch']
        ];
        $word_4 =  Word::create($data_4);

        $data_5 = [
            'user_id' => 5,
            'ru' => ['word' => 'Телефон'],
            'en' => ['word' => 'Telephone'],
            'fr' => ['word' => 'Téléphone'],
            'ja' => ['word' => '電話(Denwa)'],
            'de' => ['word' => 'Telefon']
        ];
        $word_5 =  Word::create($data_5);

        $data_6 = [
            'user_id' => 6,
            'ru' => ['word' => 'Робот'],
            'en' => ['word' => 'Robot'],
        ];
        $word_6 =  Word::create($data_6);

        $data_7 = [
            'user_id' => 7,
            'ru' => ['word' => 'Слух'],
            'en' => ['word' => 'Hearing'],
        ];
        $word_7 =  Word::create($data_7);

        $data_8 = [
            'user_id' => 8,
            'ru' => ['word' => 'Деньги'],
            'en' => ['word' => 'Money'],
        ];
        $word_8 =  Word::create($data_8);

        $data_9 = [
            'user_id' => 9,
            'ru' => ['word' => 'Зеркало'],
            'en' => ['word' => 'Mirror'],
        ];
        $word_9 =  Word::create($data_9);

        $data_10 = [
            'user_id' => 10,
            'ru' => ['word' => 'Стекло'],
            'en' => ['word' => 'Glass'],
        ];
        $word_10 =  Word::create($data_10);
    }
}
