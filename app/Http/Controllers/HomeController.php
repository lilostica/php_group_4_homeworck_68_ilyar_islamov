<?php

namespace App\Http\Controllers;

use App\Word;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $words = Word::all();
        return view('home',compact('words'));
    }
}
