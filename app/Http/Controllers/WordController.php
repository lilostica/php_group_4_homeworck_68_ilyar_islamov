<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddWordRequest;
use App\Http\Requests\WordTranslationRequest;
use App\Word;
use Illuminate\Http\Request;

class WordController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $word = Word::findOrFail($id);
        return view('word.index',compact('word'));
    }

    /**
     * @param WordTranslationRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(WordTranslationRequest $request, $id)
    {
        $word = Word::findOrFail($id);
        $user = $request->user();
        $data = [
            'user_id' => $user->id,
            'ru' => [
                'word' => $request->input('ru')
            ],
            'en' => [
                'word' => $request->input('en')
            ],
            'de' => [
                'word' => $request->input('de')
            ],
            'fr' => [
                'word' => $request->input('fr')
            ],
            'ja' => [
                'word' => $request->input('ja')
            ],
            ];
        $word->update($data);
        return redirect(route('home'))->with('status',"{$word->getRU()} translations success added!");
    }

    public function store(AddWordRequest $request)
    {
        $user = $request->user();
        $data = [
            'user_id' => $user->id,
            'ru' => [
                'word' => $request->input('ru')
            ]
            ];
        $word = new Word($data);
        $word->save();
        return back()->with('status',"{$word->getRu()} has addad!");
    }
}
