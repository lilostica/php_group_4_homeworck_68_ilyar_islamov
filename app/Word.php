<?php

namespace App;


use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Word extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['word'];
    protected $fillable = ['user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed|null
     */
    public function getRu()
    {
        if ($this->translate('ru') === null)
        {
            return null;
        }else
            {
                return $this->translate('ru')->word;
            }
    }

    /**
     * @return mixed|null
     */
    public function getJa()
    {
        if ($this->translate('ja') === null)
        {
            return null;
        }else
        {
            return $this->translate('ja')->word;
        }
    }

    /**
     * @return mixed|null
     */
    public function getEn()
    {
        if ($this->translate('en') === null)
        {
            return null;
        }else
        {
            return $this->translate('en')->word;
        }
    }

    /**
     * @return mixed|null
     */
    public function getFr()
    {
        if ($this->translate('fr') === null)
        {
            return null;
        }else
        {
            return $this->translate('fr')->word;
        }
    }

    /**
     * @return mixed|null
     */
    public function getDe()
    {
        if ($this->translate('de') === null)
        {
            return null;
        }else
        {
            return $this->translate('de')->word;
        }
    }
}
