<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'language'],function () {
    Route::get('/','HomeController@index');
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/word/{id}','WordController@index')->name('show-word');
    Route::put('update/word/{id}','WordController@update')->name('update-word');
    Route::post('/store/word','WordController@store')->name('store-word');
});

Route::get('language/{locale}', 'LanguageSwitcherController@switcher')
    ->name('language.switcher')
    ->where('locale', 'en|ru');
